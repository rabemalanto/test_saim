﻿using Domain.Abstract;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class SAIMRepository : ISaimRepository
    {

        private SAIMDBContext context = new SAIMDBContext();
        public IQueryable<Societe> societes => context.Societe;

        public IQueryable<Client> clients => context.Client;

        public void addUpdateClient(Client s)
        {
            Client clExist = context.Client.FirstOrDefault(_=>_.id == s.id);
            if (clExist != null)
            {
                clExist.nom = s.nom;
                clExist.prenom = s.prenom;
                clExist.societe_id = s.societe_id;
                clExist.email = s.email;
                context.Entry(clExist).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                s.id = Guid.NewGuid();
                context.Client.Add(s);
            }
            context.SaveChanges();
        }

        public void addUpdateSociete(Societe s)
        {
            Societe sExist = context.Societe.FirstOrDefault(x => x.id == s.id);
            if (sExist != null)
            {
                sExist.NIF = s.NIF;
                sExist.nom = s.nom;
                sExist.raison_social = s.raison_social;
                sExist.adresse = s.adresse;
                context.Entry(sExist).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                s.id = Guid.NewGuid();
                context.Societe.Add(s);
            }
            context.SaveChanges();
        }

        public void deleteClient(Guid idClient)
        {
            Client sExist = context.Client.FirstOrDefault(x => x.id == idClient);
            if (sExist != null)
            {
                context.Entry(sExist).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
                
        }
    }
}
