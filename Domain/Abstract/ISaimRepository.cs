﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface ISaimRepository
    {
        IQueryable<Societe> societes { get; }
        IQueryable<Client> clients { get; }
        void addUpdateSociete(Societe s);
        void addUpdateClient(Client s);
        void deleteClient(Guid idClient);
    }
}
