﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Utility
{
    public class FileManager
    {
        static string ToUTF8(string s)
        {
            byte[] utf = Encoding.Default.GetBytes(s);
            string s2 = System.Text.Encoding.Unicode.GetString(utf);
            return s;
        }

        public static bool GenerateFileFromListCollection<T, U>(string FileName, T ListOfDataToExport, string separateur=";")
        {
            try
            {
                var donnees = (List<U>)(object)ListOfDataToExport;
                if (donnees != null)
                {


                    using (StreamWriter sw = new StreamWriter(FileName, false, Encoding.UTF8))
                    {
                        var d = donnees.FirstOrDefault();
                        int i = 0;
                        foreach (PropertyInfo propertyInfo in d.GetType().GetProperties().OrderBy(x => x.MetadataToken))
                        {
                            if (i == 0)
                            {
                                sw.Write(ToUTF8(propertyInfo.Name));
                                i++;
                                continue;
                            }
                            sw.Write(separateur + ToUTF8(propertyInfo.Name));
                        }
                        sw.WriteLine();
                        foreach (var donnee in donnees)
                        {
                            try
                            {
                                if (donnee?.GetType() != null && donnee.GetType().GetProperties() != null)
                                {
                                    i = 0;
                                    foreach (
                                        var propertyInfo in donnee.GetType().GetProperties().OrderBy(x => x.MetadataToken))
                                    {
                                        if (propertyInfo != null)
                                        {
                                            var o = propertyInfo.GetValue(donnee);
                                            if (i == 0)
                                            {
                                                sw.Write(ToUTF8(o?.ToString() ?? string.Empty));
                                                i++;
                                                continue;
                                            }
                                            sw.Write(separateur + ToUTF8(o?.ToString() ?? string.Empty));
                                        }
                                    }
                                    sw.WriteLine();
                                }
                            }
                            catch (Exception)
                            {

                                sw.Write(separateur + string.Empty);
                            }
                        }
                    }
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public static bool ImportFromFile<T>(string path, char separateur = ';')
        {
            if (Path.GetExtension(path).Equals(".txt"))
            {
                string[] lines = File.ReadAllLines(path);

                foreach (string line in lines)
                {
                    
                    // il faudrait une heure de plus pour finir
                }
            }
                return false;
        }
    }
}
