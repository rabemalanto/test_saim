﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entity
{
    public class Societe
    {
        public Guid id { get; set; }
        public string nom { get; set; }
        public string raison_social { get; set; }
        public string adresse { get; set; }
        public string NIF { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
    }
}
