﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entity
{
    [Table("client")]
    public class Client
    {
      public Guid id { get; set; }
	  public string nom { get; set; }
	  public string prenom { get; set; }
	  public string email { get; set; }
    [ForeignKey("Societe")]
	  public Guid societe_id { get; set; }
     public virtual Societe Societe { get; set; }
    }
}
