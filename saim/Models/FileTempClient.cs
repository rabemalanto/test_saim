﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace saim.Models
{
    public class FileTempClient
    {
        public string nom { get; set; }
        public string prenom { get; set; }
        public string email { get; set; }
        public string societeNom { get; set; }
        public string societeRaison_social { get; set; }
        public string societeAdresse { get; set; }
        public string societeNIF { get; set; }

    }
}