﻿using Domain.Abstract;
using Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace saim.Infrastructure
{
    public class SaimControllerFactory: IControllerFactory
    {
        private readonly string _controllerNamespace;
        public SaimControllerFactory(string controllerNamespace)
        {
            _controllerNamespace = controllerNamespace;
        }

        public IController CreateController(RequestContext requestContext, string controllerName)
        {
            ISaimRepository repository = new SAIMRepository();
            // Rova: Récupération du type de Controller
            Type controllerType = Type.GetType(string.Concat(_controllerNamespace, ".", controllerName, "Controller"));
            IController controller;
            // essayer si le controller a un constructeur qui prend en param le repositoryd
            try
            {
                // Rova: Late Binding pour créer une instance affin de mettre en paramètre le repository
                controller = Activator.CreateInstance(controllerType, BindingFlags.CreateInstance |
                                 BindingFlags.Public |
                                 BindingFlags.Instance |
                                 BindingFlags.OptionalParamBinding,
                                 null, new[] { repository }, null) as Controller;
            }
            catch
            {
                controller = Activator.CreateInstance(controllerType) as Controller;
            }

            return controller;
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName)
        {
            return SessionStateBehavior.Default;
        }

        public void ReleaseController(IController controller)
        {
            IDisposable disposable = controller as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }
}