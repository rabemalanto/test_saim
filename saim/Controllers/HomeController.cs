﻿using Domain.Abstract;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using saim.Models;
using AutoMapper;
using Domain.Utility;
using saim.ExtentionMethod;
using System.IO;

namespace saim.Controllers
{
    public class HomeController : Controller
    {

        private ISaimRepository repository;
        public HomeController(ISaimRepository repo)
        {
            repository = repo;
        }
        public ViewResult Index()
        {
            return View(repository.clients);
        }
        public ViewResult Ajout(Guid? idClient)
        {
            ViewBag.societe = repository.societes;
            if (idClient != null)
            {
                Client cl = repository.clients.FirstOrDefault(_=>_.id == idClient);
                if (cl != null)
                {
                    return View(cl);
                }
                return View(new Client());
            }
            return View(new Client());
        }


        public ActionResult SaveClient(Client c)
        {
            repository.addUpdateClient(c);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ViewResult AjoutSociete()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AjoutSociete(Societe soc)
        {
            repository.addUpdateSociete(soc);

            return RedirectToAction("Index");
        }
        public ActionResult Delete(Guid idClient)
        {
            repository.deleteClient(idClient);

            return RedirectToAction("Index");
        }
        public void Export()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Client, FileTempClient>().ForMember(x=>x.societeNom,g=>g.MapFrom(_=>_.Societe.nom))
                .ForMember(x => x.societeRaison_social, g => g.MapFrom(_ => _.Societe.raison_social))
                .ForMember(x => x.societeNIF, g => g.MapFrom(_ => _.Societe.NIF))
                .ForMember(x => x.societeAdresse, g => g.MapFrom(_ => _.Societe.adresse))
                ;
            });
            IMapper mapper = config.CreateMapper();
            IList<FileTempClient> tempObjs = new List<FileTempClient>();

            foreach (Client c in repository.clients.ToList())
            {
                FileTempClient f = mapper.Map<Client, FileTempClient>(c);
                tempObjs.Add(f);
            }


            string fileName = GenerateRandomString.RandomString(12) + ".txt";
            string path = System.Configuration.ConfigurationManager.AppSettings["FileBasePath"] + fileName;
            bool isPass = FileManager.GenerateFileFromListCollection<IList<FileTempClient>, FileTempClient>(path, tempObjs);
            Response.ForceDownload(path, fileName);

        }
        public ActionResult Import(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string fileName = Path.GetFileName(file.FileName);
                    string path = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["FileBasePathUpload"],
                                               fileName);
                    file.SaveAs(path);
                    
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return RedirectToAction("Index");
        }

    }
}