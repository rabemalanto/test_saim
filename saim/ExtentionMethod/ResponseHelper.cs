﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace saim.ExtentionMethod
{
    public static class ResponseHelper
    {
        public static void ForceDownload(this HttpResponseBase Response, string fullPathToFile, string outputFileName)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + outputFileName);
            Response.WriteFile(fullPathToFile);
            Response.ContentType = "";
            Response.End();
        }
    }
}